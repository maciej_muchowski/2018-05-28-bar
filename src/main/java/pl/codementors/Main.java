package pl.codementors;

import pl.codementors.model.Bar;
import pl.codementors.model.Barman;
import pl.codementors.model.Client;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class for bar app.
 *
 * @author Maciek
 */
public class Main {

    private static Bar bar = new Bar();
    private static Barman barman = new Barman(bar);
    private static Scanner inputScanner = new Scanner(System.in);
    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());

    private static Client client1 = new Client("Client 1", bar);
    private static Client client2 = new Client("Client 2", bar);
    private static Client client3 = new Client("Client 3", bar);
    private static Client client4 = new Client("Client 4", bar);
    private static Client client5 = new Client("Client 5", bar);

    public static void main(String[] args) {
        System.out.println("Welcome to the Bar app. Type 'quit' as drink name to exit.");
        Thread th1 = new Thread(barman);
        Thread th2 = new Thread(client1);
        Thread th3 = new Thread(client2);
        Thread th4 = new Thread(client3);
        Thread th5 = new Thread(client4);
        Thread th6 = new Thread(client5);
        th1.start();
        th2.start();
        th3.start();
        th4.start();
        th5.start();
        th6.start();

        while (true) {
            System.out.print("Enter drink name: ");
            String line = inputScanner.nextLine();
            if (line.equals("quit")) {
                break;
            }
            barman.setDrinkName(line);
            barman.setOrder(true);
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        barman.stop();
        client1.stop();
        client2.stop();
        client3.stop();
        client4.stop();
        client5.stop();

        th1.interrupt();
        th2.interrupt();
        th3.interrupt();
        th4.interrupt();
        th5.interrupt();
        th6.interrupt();
    }
}
