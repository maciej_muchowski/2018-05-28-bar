package pl.codementors.model;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class describing Client.
 *
 * @author Maciek
 */
public class Client implements Runnable {
    /**
     * Client's name.
     */
    private String name;
    /**
     * Bar in which client is.
     */
    private Bar bar;
    /**
     * Boolean that keeps Client running.
     */
    private boolean running = true;
    /**
     * Logger.
     */
    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());

    public Client(String name, Bar bar) {
        this.name = name;
        this.bar = bar;
    }

    @Override
    public void run() {
        while (running) {
            try {
                String drink = bar.takeDrink();
                int drinkingTime = drink.length();
                System.out.println(name + " drinks " + drink);
                Thread.sleep(drinkingTime);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }
    /**
     * Method stopping Client's work.
     */
    public void stop() {
        this.running = false;
    }
}
