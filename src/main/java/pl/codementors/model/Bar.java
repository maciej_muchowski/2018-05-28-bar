package pl.codementors.model;

public class Bar {

    /**
     * Drink representation.
     */
    private String drink = null;

    /**
     * Method putting drink on bar.
     * @param drink Drink to be put.
     */
    public synchronized void putDrink(String drink) {
        this.drink = (drink);
        notifyAll();
    }

    /**
     * Method taking drink from bar.
     * @return Taken drink.
     * @throws InterruptedException Happens when 'wait' is interrupted.
     */
    public synchronized String takeDrink() throws InterruptedException {
        while (drink == null) {
            this.wait();
        }
        String ret = (drink);
        drink = null;
        return ret;
    }
}
