package pl.codementors.model;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class describing Barman
 *
 * @author Maciek
 */
public class Barman implements Runnable {
    /**
     * Logger.
     */
    private static final Logger log = Logger.getLogger(Barman.class.getCanonicalName());
    /**
     * Boolean that keeps barman working.
     */
    private boolean running = true;
    /**
     * Boolean to check if order was made.
     */
    private boolean order = false;
    /**
     * Name of drink to make.
     */
    private String drinkName;
    /**
     * Bar in which Barman is working.
     */
    private Bar bar;

    public Barman(Bar bar) {
        this.bar = bar;
    }

    public void setOrder(boolean order) {
        this.order = order;
    }

    public void setDrinkName(String drinkName) {

        this.drinkName = drinkName;
    }

    @Override
    public void run() {
        while (running) {
            try {
                if (order){
                    bar.putDrink(drinkName);
                }
                order = false;
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                log.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }

    /**
     * Method stopping Barman's work.
     */
    public void stop() {
        this.running = false;
    }
}
